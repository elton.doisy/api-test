const express = require('express');
const app = express();
require('./models/dbConfig');
const postsRoutes = require('./routes/usersController');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.set('useFindAndModify', false);

app.use(bodyParser.json());
app.use(cors());
app.use('/users', postsRoutes);

app.listen(5600, () => console.log('Server started: 5600'));