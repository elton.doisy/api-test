const request = require('supertest');
const { postUser, putUser, getUser, externalTable } = require('./mock/users');
const { baseUrl } = require('./mock/globalVar');
const { deleteRow, putRow } = require('./mock/sqlResult');

describe(`${baseUrl}/users/`, () => {
  describe(`GET users/ =>`, () => {
    test(" status 200", async () => {
      const res = await request(baseUrl)
        .get('/users/')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`POST users/ =>`, () => {
    test("Insert new user ", async () => {
      const res = await request(baseUrl)
        .post('/users')
        .set('Accept', 'application/json')
        .send(postUser);

      expect(res.statusCode).toBe(200);
      getUser.id = res.body.insertId;
    });
  });

  describe(`GET users/:id =>`, () => {
    test(`Get a user with id`, async () => {
      const res = await request(baseUrl)
        .get(`/users/${getUser.id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body[0])).toBe(JSON.stringify({getUser}));
    });
  });

  describe(`PUT users/update/:id =>`, () => {
    test(`Update user with specific body`, async () => {
      const res = await request(baseUrl)
        .put(`/users/update/${getUser.id}`)
        .set('Accept', 'application/json')
        .send(putUser);

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putRow));
    });
  });

  describe(`DELETE users/delete/:id =>`, () => {
    test(`Delete a user with id`, async () => {
      const res = await request(baseUrl)
        .delete(`/users/user/${getUser.id}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(deleteRow)).toBe(JSON.stringify(res.body));
    });
  });
});
