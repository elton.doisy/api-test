const postUser = {
  _id: "93bc418995abdec115cce67c",
  email: "test@email.com",
  password: 'testPassword',
  firstName: 'Super',
  lastName: 'Test',
  address: 'Address',
  city: 'City',
  postalCode: 'azerty',
};

const putUser = {
  _id: "62bc418995abdec115cce67c",
  email: "elton@gmail.com",
  address: "60 rue de paris",
  city: "paris",
  lastName: "john",
  firstName: "elton",
  created_at: "2022-06-29T12:31:50.990Z"
}

const getUser = {
  _id: "62bc418995abdec115cce67c",
  email: "elton@gmail.com",
  address: "60 rue de paris",
  city: "paris",
  lastName: "doisy",
  firstName: "elton",
  created_at: "2022-06-29T12:31:50.990Z"
}

const externalTable = {
  name: 'test_image',
  title: 'Free',
  price: 0
}

module.exports = {
  postUser,
  putUser,
  getUser,
  externalTable
}
