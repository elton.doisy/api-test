const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId;

const { UsersModel } = require('../models/usersModel');

router.get('/', (req, res) => {
  UsersModel.find((err, docs) => {
    if (!err) res.send(docs);
    else console.log("Error to get data : " + err);
  })
});

router.get('/:id', (req, res) => {
  UsersModel.findById(req.params.id ,(err, docs) => {
    if (!err) res.send(docs);
    else console.log("Error to get data : " + err);
  })
});

router.post('/', (req, res) => {
  const newRecord = new UsersModel({
    email: req.body.email,
    city: req.body.city,
    lastName: req.body.lastName,
    firstName: req.body.firstName,
    password: req.body.password,
  });

  newRecord.save((err, docs) => {
    if (!err) res.send(docs);
    else console.log('Error creating new data : ' + err);
  })
});
// update
router.put("user/update/:id", (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknow : " + req.params.id)
  
  const updateRecord = {
    email: req.body.email,
    city: req.body.city,
    lastName: req.body.lastName,
    firstName: req.body.firstName,
  };

  UsersModel.findByIdAndUpdate(
    req.params.id,
    { $set: updateRecord},
    { new: true },
    (err, docs) => {
      if (!err) res.send(docs);
      else console.log("Update error : " + err);
    }
  )
});


router.delete("user/delete/:id", (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknow : " + req.params.id)
  
    UsersModel.findByIdAndRemove(
    req.params.id,
    (err, docs) => {
      if (!err) res.send(docs);
      else console.log("Delete error : " + err);
    })
});

module.exports = router;