module.exports = {
  apps : [{
    name: "node-api",
    script: "./index.js",
    exec_mode: "fork",
    watch: true
  }]
}
