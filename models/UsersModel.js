const mongoose = require("mongoose");

const UsersModel = mongoose.model(
  "node-api",
  {
    email: {
      type: String,
      required: true
    },
    address: {
      type: String,
      
    },
    created_at: {
      type: Date,
      default: Date.now
    },
    city: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    }
  },
  "users"
);

module.exports = { UsersModel };